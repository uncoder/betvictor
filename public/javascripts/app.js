var app = angular.module('Betvictor', ['ngResource']);

app.factory('Sport', [
  '$resource', function($resource) {
    return $resource('/sports/:sportId', {}, {
      all: {
        isArray: true
      },
      getEvents: {
        isArray: true
      }
    });
  }
]);

app.factory('Event', [
  '$resource', function($resource) {
    return $resource('/sports/:sportId/events/:eventId', {}, {
      getOutcomes: {
        isArray: true
      }
    });
  }
]);

app.controller('BetCtrl', [
  '$scope', 'Sport', 'Event', function($scope, Sport, Event) {
    $scope.languages = {
      "en": "English",
      "de": "Deutsch",
      "zh-cn": "简体中文"
    }
    $scope.cur_lang = "en"

    $scope.init = function () {
      $scope.loadSports();
      $scope.current_sport = 0;
      $scope.current_event = 0;
      setInterval($scope.loadSports, 10000)
    };

    $scope.loadSports = function () {
      Sport.query({ lang: $scope.cur_lang }, function(data) {
        $scope.sports = data
      });
    };

    $scope.loadEvents = function (sportId) {
      $scope.current_sport = sportId;
      $scope.events = Sport.getEvents({ sportId: sportId, lang: $scope.cur_lang });
      $scope.outcomes = [];
    };

    $scope.loadOutcomes = function (sportId, eventId) {
      $scope.outcomes = Event.getOutcomes({ sportId: sportId, eventId: eventId, lang: $scope.cur_lang });
      $scope.current_event = eventId;
    };

    $scope.changeLang = function (lang) {
      $scope.cur_lang = lang
      $scope.outcomes = [];
      $scope.events = [];
      $scope.current_sport = 0;
      $scope.current_event = 0;
      $scope.loadSports();
    };
  }
]);
