$LOAD_PATH << File.expand_path(File.dirname(__FILE__))

require "bundler"
Bundler.require

Dir['lib/**/*.rb'].each do |file|
  require file
end

require "betvictor"
