class Betvictor < Sinatra::Base
  set public_folder: "public", static: true
  set json_url: "http://www.betvictor.com/live/:lang/live/list.json", default_lang: "en"

  before(/^\/.+$/) do
    @store = JsonStore.new(settings.json_url, params[:lang])
  end

  # Index page
  get "/" do
    erb :index
  end

  # Sports json
  get "/sports" do
    content_type :json
    @store.sports.to_json
  end

  # Events json
  get "/sports/:id" do
    content_type :json
    @store.events(params[:id]).to_json
  end

  # Outcomes json
  get "/sports/:sport_id/events/:id" do
    content_type :json
    @store.outcomes(params[:sport_id], params[:id]).to_json
  end
end
