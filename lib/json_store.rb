require 'json'
require 'open-uri'

class JsonStore
  def initialize(json_url, lang)
    @data = load_json(make_url(json_url, lang))
  end

  def sports
    @data["sports"].map do |sport|
      {
        id: sport["id"],
        title: sport["title"],
        pos: sport["pos"],
        events_count: (sport["events"]) ? sport["events"].size : 0
      }
    end
  end

  def events(sport_id)
    sport = get_sport(sport_id)
    return [] unless sport
    sport["events"].map do |event|
      {
        id: event["id"],
        title: event["title"],
        pos: event["pos"],
        home_team: event["home_team"],
        away_team: event["away_team"]
      }
    end
  end

  def outcomes(sport_id, event_id)
    event = get_event(sport_id, event_id)
    return [] unless event
    event["outcomes"].map do |outcome|
      {
        id: outcome["id"],
        description: outcome["description"],
        price: outcome["price"],
        pos: outcome["print_order"]
      }
    end
  end

  private

  def load_json(json_url)
    JSON.load(open(json_url)) rescue {}
  end

  def get_sport(sport_id)
    @data["sports"].detect { |sport| sport["id"] == sport_id.to_i } if @data["sports"]
  end

  def get_event(sport_id, event_id)
    sport = get_sport(sport_id)
    sport["events"].detect { |event| event["id"] == event_id.to_i } if sport && sport["events"]
  end

  def make_url(json_url, lang)
    lang ||= "en"
    json_url.gsub(":lang", lang)
  end
end
