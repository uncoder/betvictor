require File.expand_path '../spec_helper.rb', __FILE__

describe JsonStore do
  let :test_data do
    {
      "sports" => [
        { "id" => 1, "title" => "Sport 1", "pos" => 2 },
        { "id" => 2, "title" => "Sport 2", "pos" => 1,
          "events" => [
            { "id" => 100, "title" => "Event 1", "pos" => 2, "home_team" => "Team 1", "away_team" => "Team 2" },
            { "id" => 200, "title" => "Event 2", "pos" => 1, "home_team" => "Team 1", "away_team" => "Team 2",
              "outcomes" => [
                { "id" => 111, "description" => "Description 1", "price" => "1/5", "print_order" => 2},
                { "id" => 222, "description" => "Description 2", "price" => "5/1", "print_order" => 1}
              ]
            }
          ]
        }
      ]
    }
  end

  let :store do
    JsonStore.new("", nil)
  end

  before :each do
    allow_any_instance_of(JsonStore).to receive(:load_json).and_return(test_data)
  end

  describe "#initialize" do
    it "loads object to @data" do
      expect(store.instance_variable_get(:@data)).to eq(test_data)
    end
  end

  describe "private" do
    describe "#get_sport" do
      it "returns sport node" do
        expect(store.send(:get_sport, 1)).to eq(test_data["sports"].first)
      end

      it "returns nil when not found" do
        expect(store.send(:get_sport, 3)).to be_nil
      end
    end

    describe "#get_event" do
      it "returns event node" do
        expect(store.send(:get_event, 2, 100)).to eq(test_data["sports"].last["events"].first)
      end

      it "returns nil when not found" do
        expect(store.send(:get_event, 2, 101)).to be_nil
      end
    end

    describe "#make_url" do
      it "returns en ur by default" do
        expect(store.send(:make_url, "/:lang/", nil)).to eq("/en/")
      end

      it "returns url by language" do
        expect(store.send(:make_url, "/:lang/", "de")).to eq("/de/")
      end
    end
  end

  describe "#sports" do
    it "returns sports array" do
      expect(store.sports).to eq([
        { id: 1, title: "Sport 1", pos: 2, events_count: 0 },
        { id: 2, title: "Sport 2", pos: 1, events_count: 2 }
      ])
    end
  end

  describe "#events" do
    it "returns events array" do
      expect(store.events(2)).to eq([
        { id: 100, title: "Event 1", pos: 2, home_team: "Team 1", away_team: "Team 2" },
        { id: 200, title: "Event 2", pos: 1, home_team: "Team 1", away_team: "Team 2" }
      ])
    end

    it "returns [] when not found" do
      expect(store.events(3)).to eq([])
    end
  end

  describe "#outcomes" do
    it "returns outcomes array" do
      expect(store.outcomes(2, 200)).to eq([
        { id: 111, description: "Description 1", price: "1/5", pos: 2 },
        { id: 222, description: "Description 2", price: "5/1", pos: 1 }
      ])
    end

    it "returns [] when not found" do
      expect(store.outcomes(1, 200)).to eq([])
    end
  end
end
