require File.expand_path '../spec_helper.rb', __FILE__

describe "Betvictor application" do
  before :each do
    allow_any_instance_of(JsonStore).to receive(:load_json).and_return({})
  end

  let :test_data do
    [ { test: "data"} ]
  end

  describe "GET /" do
    it "resonds successfully" do
      get '/'
      expect(last_response).to be_ok
    end
  end

  describe "GET /sports" do
    before :each do
      allow_any_instance_of(JsonStore).to receive(:sports).and_return(test_data)
    end

    it "resonds successfully" do
      get '/sports'
      expect(last_response).to be_ok
    end

    it "returns sports array" do
      get '/sports'
      expect(last_response.body).to eq(test_data.to_json)
    end
  end

  describe "GET /sports/:id" do
    before :each do
      allow_any_instance_of(JsonStore).to receive(:events).and_return(test_data)
    end

    it "resonds successfully" do
      get '/sports/1'
      expect(last_response).to be_ok
    end

    it "returns sports array" do
      get '/sports/1'
      expect(last_response.body).to eq(test_data.to_json)
    end
  end

  describe "GET /sports/:sport_id/events/:id" do
    before :each do
      allow_any_instance_of(JsonStore).to receive(:outcomes).and_return(test_data)
    end

    it "resonds successfully" do
      get '/sports/1/events/1'
      expect(last_response).to be_ok
    end

    it "returns sports array" do
      get '/sports/1/events/1'
      expect(last_response.body).to eq(test_data.to_json)
    end
  end
end
