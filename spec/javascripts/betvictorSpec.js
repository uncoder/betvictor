describe("Betvictor", function() {
  var scope, controller;

  beforeEach(function () {
    module('Betvictor');
  });

  sports = [
    { id: 1, title: 'Football', pos: 1 },
    { id: 2, title: 'Tennis', pos: 2 }
  ]

  events = [
    { id: 100, title: 'Event 1', pos: 1, events_count: 0 },
    { id: 200, title: 'Event 2', pos: 2, events_count: 1 }
  ]

  outcomes = [
    { id: 100, description: 'Outcome 1', price: "1", pos: 2 },
    { id: 200, description: 'Outcome 2', price: "2", pos: 1 }
  ]

  SportMock = {
    query: function (params) {
      return sports;
    },
    getEvents: function (params) {
      return events;
    }
  }

  EventMock = {
    getOutcomes: function (params) {
      return outcomes;
    }
  }

  describe('BetCtrl', function () {
    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      controller = $controller('BetCtrl', {
        '$scope': scope, 'Sport': SportMock, 'Event': EventMock
      });
    }));

    describe('cur_lang', function () {
      it('sets default language', function () {
        expect(scope.cur_lang).toEqual("en");
      });
    });

    describe('loadSports()', function () {
      it('loads sports', function () {
        spyOn(SportMock, 'query').and.callFake(function() {
          scope.sports = sports;
        });
        scope.loadSports();
        expect(scope.sports).toEqual(sports);
      });

      it('pass lang param', function () {
        spyOn(SportMock, 'query')
        scope.loadSports();
        expect(SportMock.query).toHaveBeenCalledWith({ lang: "en" }, jasmine.any(Function));
      });
    });

    describe('loadEvents()', function () {
      it('loads events', function () {
        scope.loadEvents(100);
        expect(scope.events).toEqual(SportMock.getEvents());
      });

      it('clean outcomes', function () {
        scope.loadEvents(100);
        expect(scope.outcomes).toEqual([]);
      });

      it('sets current_sport', function () {
        scope.loadEvents(100);
        expect(scope.current_sport).toEqual(100);
      });

      it('pass lang param', function () {
        spyOn(SportMock, 'getEvents')
        scope.loadEvents(100);
        expect(SportMock.getEvents).toHaveBeenCalledWith({ sportId: 100, lang: "en" });
      });
    });

    describe('loadOutcomes()', function () {
      it('loads outcomes', function () {
        scope.loadOutcomes(100, 100);
        expect(scope.outcomes).toEqual(EventMock.getOutcomes());
      });

      it('sets current_event', function () {
        scope.loadOutcomes(200, 300);
        expect(scope.current_event).toEqual(300);
      });

      it('pass lang param', function () {
        spyOn(EventMock, 'getOutcomes')
        scope.loadOutcomes(200, 300);
        expect(EventMock.getOutcomes).toHaveBeenCalledWith({ sportId: 200, eventId: 300, lang: "en" });
      });
    });

    describe('init()', function () {
      it('loads sports', function () {
        spyOn(SportMock, "query");
        scope.init();
        expect(SportMock.query).toHaveBeenCalled();
      });

      it('sets current_sport', function () {
        spyOn(SportMock, "query");
        scope.init();
        expect(scope.current_sport).toEqual(0);
      });

      it('sets current_event', function () {
        spyOn(SportMock, "query");
        scope.init();
        expect(scope.current_event).toEqual(0);
      });
    });

    describe('changeLang()', function () {
      it('sets cur_lang', function () {
        scope.changeLang("de");
        expect(scope.cur_lang).toEqual("de")
      });

      it('clean event and outcomes', function () {
        scope.loadEvents(100);
        scope.changeLang("de");
        expect(scope.events).toEqual([]);
        expect(scope.outcomes).toEqual([]);
      });

      it('clean current_sport and current_event', function () {
        scope.loadEvents(100);
        scope.changeLang("de");
        expect(scope.current_sport).toEqual(0);
        expect(scope.current_event).toEqual(0);
      });

      it('calls load_sport()', function () {
        spyOn(scope, "loadSports");
        scope.changeLang("de");
        expect(scope.loadSports).toHaveBeenCalled();
      });
    });
  });

});
