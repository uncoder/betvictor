# Betvictor interview task

Basic usage:

* Run application ```rackup config.ru```
* Run Rsec tests ```rake specs```
* Run Jasmine tests ```rake jasmine```